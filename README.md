El proyecto esta creado con `CRA(Create React APP)`, usando la plantilla de `Redux` y `Redux-Toolkit`. Usa `styled-components` para darle estilo a los componentes y `redux-persist` para cachear en el LocalStorage las llamadas a la API y evitar llamadas innecesarias.

## Scripts

Despues de `npm install`, puedes usar:

`npm start` Levanta la app en desarrollo.

`npm test` Lanza los tests en watch mode. 

`npm run build` Genera la build optimizada para produccion.

`npm run lint` Lanza el linter para comprobar que sigue las reglas definidas.


## Proyecto

He creado un MVP(minimum viable product), empezando con una vista con una lista de todos los moviles, que podria desplegarse a produccion y he ido añadiendo valor con cada commit.

#### Features

Hay 5 principales features:
- Ver una lista con todos los moviles.
- Poder filtrar por marca y/o modelo la lista de todos los moviles.
- Ver un movil en una vista detallada.
- Poder añadir el movil al carrito.
- Cachea las llamadas en localStorage durante 1 hora.

#### Vistas
##### Catalogo de telefonos moviles
En esta pagina se renderiza una lista con todos los telefonos moviles, que provienen o de la API o de la cache en caso de que ya se haya entrado. Se muestra una card por cada movil donde se incluye la foto, marca y modelo y el precio.

En la esquina superior derecha hay una caja de busqueda para poder filtrar por marca y/o modelo en tiempo real.

Haciendo click en cada card te lleva a la vista detallada.


##### Mobile Phone Details
En esta pagina el usuario puede ver dos columnas, la foto del movil y todos los detalles del modelo. Debajo de los detalles el usuario puede seleccionar el color y la capacidad entre las diferentes opciones para luego poder añadirlo al carrito de la compra, y este se actualiza de inmediato cuando la API valida que es correcto el modelo, color y almacenamiento.

En la vista responsive de movil la imagen se coloca debajo del titulo formando una columna.

Al igual que la vista anterior, los datos provienen de la API la primera vez que entra o de la cache.


#### Header
En ambas paginas en comun hay un Header que incluye el logo de la App, un breadcrumb que permite volver a la homepage y un carrito de la compra donde el usuario puede ver cuantos productos ha añadido.



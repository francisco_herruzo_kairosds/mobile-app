import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "./store";
import App from "./App";

describe("App", () => {
  it("should render the catalogue page when the App render", () => {
    const { getAllByText } = render(
      <Provider store={store}>
        <App />
      </Provider>
    );

    expect(getAllByText(/Mobile Catalogue/i)).toHaveLength(2);
  });
});

export const API_PATH = "https://front-test-api.herokuapp.com";
export const LOCALSTORAGE_DATA_EXPIRATION = 60 * 60 * 1000;

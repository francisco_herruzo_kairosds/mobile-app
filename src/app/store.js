import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import {
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";
import expireIn from "redux-persist-transform-expire-in";
import { LOCALSTORAGE_DATA_EXPIRATION } from "./constants";
import rootReducer from "./rootReducer";

const persistConfig = {
  key: "root",
  storage,
  transforms: [expireIn(LOCALSTORAGE_DATA_EXPIRATION, "expirationKey", [])],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

if (process.env.NODE_ENV !== "production" && module.hot) {
  module.hot.accept("./rootReducer", () => store.replaceReducer(rootReducer));
}

export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware({
    serializableCheck: {
      ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    },
  }),
});

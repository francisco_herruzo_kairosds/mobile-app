import { combineReducers } from "@reduxjs/toolkit";
import mobilePhonesReducer from "../features/mobilePhones/mobilePhonesSlice";
import searchReducer from "../features/search/searchSlice";
import cartReducer from "../features/cart/cartSlice";

const rootReducer = combineReducers({
  mobilePhones: mobilePhonesReducer,
  search: searchReducer,
  cart: cartReducer,
});

export default rootReducer;

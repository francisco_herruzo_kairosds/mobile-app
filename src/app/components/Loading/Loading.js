import Loader from "react-loader-spinner";
import styled from "styled-components";

const Loading = () => {
  return (
    <LoaderWrapper data-testid="Loader">
      <Loader type="TailSpin" color="#9CA3AF" height={100} width={100} />
    </LoaderWrapper>
  );
};

const LoaderWrapper = styled.div`
  margin: 50px auto;
  width: 100%;
  display: block;
`;

export default Loading;

import { HomeIcon } from "@heroicons/react/solid";
import { useHistory } from "react-router-dom";
import styled from "styled-components";

const Breadcrumb = ({ children }) => {
  const history = useHistory();
  const handleClick = () => history.push("/");

  return (
    <BreadcrumbWrapper>
      <HomeIcon onClick={handleClick} /> / {children}
    </BreadcrumbWrapper>
  );
};

const BreadcrumbWrapper = styled.div`
  display: flex;
  align-items: center;
  text-decoration: none;
  padding-left: 5px;
  color: #4b5563;
  font-weight: 700;
  > svg {
    color: #1f2937;
    width: 24px;
    cursor: pointer;
  }
`;

export default Breadcrumb;

import React from "react";
import { render } from "@testing-library/react";
import Heading from "./Heading";

describe("Heading", () => {
  it("should render the passed children", () => {
    const headingText = "Title";
    const { getByText } = render(<Heading>{headingText}</Heading>);

    expect(getByText(headingText)).toBeInTheDocument();
  });
});

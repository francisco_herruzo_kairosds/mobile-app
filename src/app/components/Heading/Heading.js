import styled from "styled-components";

const Heading = ({ children }) => {
  return <HeadingTag>{children}</HeadingTag>;
};

const HeadingTag = styled.h1`
  font-size: 30px;
  font-weight: 700;
  color: #111827;
  margin-top: 58px;
  margin-bottom: 44px;
`;
export default Heading;

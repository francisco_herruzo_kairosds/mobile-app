import React from "react";
import { render } from "@testing-library/react";
import NavBar from "./NavBar";
import logo from "../../images/logo.svg";

describe("NavBar", () => {
  it("should render the NavBar", () => {
    const { getByText } = render(<NavBar logo={logo} />);

    expect(getByText(/BreadCrumb/i)).toBeInTheDocument();
  });
  it("should render the logo in the NavBar", () => {
    const { getByAltText } = render(<NavBar logo={logo} />);

    expect(getByAltText(/Logo/i)).toBeInTheDocument();
  });
  it("should render the right breadcrumb in the NavBar", () => {
    const { getByText } = render(<NavBar logo={logo} />);

    expect(getByText(/Home/i)).toBeInTheDocument();
  });
});

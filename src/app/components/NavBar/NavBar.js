import styled from "styled-components";
import { useSelector } from "react-redux";
import { ShoppingCartIcon } from "@heroicons/react/solid";

const NavBar = ({ logo, children }) => {
  const numberOfItemInCart = useSelector((state) => state.cart.count);
  return (
    <NavBarWrapper>
      <Logo>
        <img src={logo} alt="Logo" />
      </Logo>
      <>{children}</>
      <ShoppingCart>
        <ShoppingCartIcon color={"#9CA3AF"} />
        <ShoppingCartNumber>{numberOfItemInCart}</ShoppingCartNumber>
      </ShoppingCart>
    </NavBarWrapper>
  );
};

const NavBarWrapper = styled.div`
  margin: 0 2em;
  padding: 20px 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 2px solid #e2e8f0;
`;
const Logo = styled.div`
  color: purple;
  text-align: center;
  > img {
    width: 48px;
    height: 48px;
  }
`;

const ShoppingCart = styled.div`
  position: relative;
  align-items: center;
  border-radius: 3px;
  > svg {
    width: 30px;
  }
`;
const ShoppingCartNumber = styled.div`
  font-size: 16px;
  position: absolute;
  top: -9px;
  left: 20px;
  background: #374151;
  color: white;
  font-weight: 700;
  padding: 1px 5px;
  border-radius: 50%;
`;

export default NavBar;

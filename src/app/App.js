import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import MobileCatalogue from "../pages/MobileCatalogue";
import SingleMobilePhone from "../pages/SingleMobilePhone";

import "./styles/App.css";

const App = () => {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route exact path="/" component={MobileCatalogue} />
          <Route
            exact
            path="/details/:mobilePhoneId"
            component={SingleMobilePhone}
          />
          <Redirect to="/" />
        </Switch>
      </div>
    </Router>
  );
};

export default App;

import { createSlice } from "@reduxjs/toolkit";
export const selectSearchTerm = (state) => state.search.value;

const searchSlice = createSlice({
  name: "search",
  initialState: { value: "" },
  reducers: {
    modify: (state, action) => {
      state.value = action.payload;
    },
  },
});

export const { modify } = searchSlice.actions;

export default searchSlice.reducer;

import { store } from "../../app/store";
import { modify } from "./searchSlice";

describe("Search Slice", () => {
  it("should update the search term when the modify action is dispatched", () => {
    let state = store.getState().search;
    const expectedSearchTerm = "Acer";

    store.dispatch(modify(expectedSearchTerm));
    state = store.getState().search;

    expect(state.value).toBe(expectedSearchTerm);
  });
});

import { useEffect } from "react";
import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchMobilePhoneById,
  selectMobilePhoneById,
} from "./mobilePhonesSlice";
import SingleMobilePhoneDetails from "./SingleMobilePhoneDetails";

const MobilePhoneDetailsContainer = ({ mobilePhoneId }) => {
  const dispatch = useDispatch();
  const status = useSelector((state) => state.mobilePhones.status);
  const mobilePhone = useSelector((state) =>
    selectMobilePhoneById(state, mobilePhoneId)
  );

  useEffect(() => {
    dispatch(fetchMobilePhoneById(mobilePhoneId));
  }, [dispatch, mobilePhoneId]);

  return (
    <SingleMobilePhoneDetailsWrapper>
      <SingleMobilePhoneDetails status={status} mobilePhone={mobilePhone} />
    </SingleMobilePhoneDetailsWrapper>
  );
};

const SingleMobilePhoneDetailsWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 800px;
  padding: 20px;
  border: 1px solid #e2e8f0;
  margin: 100px auto;
  --tw-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000),
    var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
  @media (max-width: 1024px) {
    border: none;
    box-shadow: none;
    width: 700px;
  }
  @media (max-width: 736px) {
    width: 400px;
    margin: 0 auto;
    padding: 0;
  }
`;

export default MobilePhoneDetailsContainer;

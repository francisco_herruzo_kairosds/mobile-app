import styled from "styled-components";
import MobilePhoneCard from "./MobilePhoneCard";
import Loading from "../../app/components/Loading/Loading";

const MobilePhonesList = ({ status, mobilePhones }) => {
  return (
    <MobilePhonesListWrapper>
      {status === "loading" && <Loading />}
      {mobilePhones &&
        mobilePhones.map(({ id, imgUrl, brand, model, price }) => (
          <MobilePhoneCard
            key={id}
            id={id}
            img={imgUrl}
            brand={brand}
            model={model}
            price={price}
          />
        ))}
    </MobilePhonesListWrapper>
  );
};

const MobilePhonesListWrapper = styled.div`
  max-width: 1400px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: center;
  margin: 0 auto;
  @media (max-width: 736px) {
    justify-content: center;
  }
`;

export default MobilePhonesList;

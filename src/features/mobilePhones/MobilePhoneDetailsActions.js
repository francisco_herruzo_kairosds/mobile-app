import { useState } from "react";
import { nanoid } from "@reduxjs/toolkit";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { addItemToCart } from "../cart/cartSlice";

const MobilePhoneDetailsActions = ({ id, storages, colors }) => {
  const [colorSelected, setColor] = useState({ color: "", key: "" });
  const [storageSelected, setStorage] = useState({ storage: "", key: "" });
  const dispatch = useDispatch();

  const onAddToCartClicked = async () => {
    if (colorSelected && storageSelected)
      await dispatch(
        addItemToCart({
          id,
          color: colorSelected.key,
          storage: storageSelected.key,
        })
      );
  };

  return (
    <MobilePhoneDetailsActionsWrapper>
      <Options>
        <ColorSelector>
          <SelectorTitle>Color:</SelectorTitle>
          {colors.map((color, colorKey) => {
            return (
              <div key={nanoid()}>
                <input
                  type="radio"
                  id={colorKey}
                  name="color"
                  value={color}
                  defaultChecked={color === colorSelected.color}
                  onClick={(event) =>
                    setColor({
                      key: event.target.id,
                      color: event.target.value,
                    })
                  }
                />
                <label htmlFor={color}>{color}</label>
              </div>
            );
          })}
        </ColorSelector>
        <StorageSelector>
          <SelectorTitle>Storage:</SelectorTitle>
          {storages.map((storage, index) => {
            return (
              <div key={nanoid()}>
                <input
                  type="radio"
                  id={index}
                  name="storage"
                  value={storage}
                  defaultChecked={storage === storageSelected.storage}
                  onClick={(event) =>
                    setStorage({
                      key: event.target.id,
                      storage: event.target.value,
                    })
                  }
                />
                <label htmlFor={storage}>{storage}</label>
              </div>
            );
          })}
        </StorageSelector>
      </Options>

      <CTAAddToCart onClick={onAddToCartClicked}>Add to Cart</CTAAddToCart>
    </MobilePhoneDetailsActionsWrapper>
  );
};

const MobilePhoneDetailsActionsWrapper = styled.div``;
const Options = styled.div`
  display: flex;
  color: #111827;
  justify-content: space-evenly;
  margin-bottom: 40px;
`;
const SelectorTitle = styled.div`
  font-size: 14px;
  color: #111827;
  font-weight: 700;
  padding-bottom: 5px;
`;
const ColorSelector = styled.div`
  font-size: 14px;
  color: #111827;
  font-weight: 500;
`;
const StorageSelector = styled.div`
  font-size: 14px;
  font-weight: 500;
`;
const CTAAddToCart = styled.div`
  display: inline-block;
  color: white;
  padding: 10px 20px;
  font-weight: 500;
  font-size: 14px;
  background-color: #3b82f6;
  border-radius: 5px;
  transition: background-color 0.2s ease;
  cursor: pointer;
  --tw-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06);
  box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000),
    var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);

  :hover {
    background-color: #1d4ed8;
  }
`;

export default MobilePhoneDetailsActions;

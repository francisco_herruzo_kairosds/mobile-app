import styled from "styled-components";
import MobilePhoneDetailsActions from "./MobilePhoneDetailsActions";
import Loading from "../../app/components/Loading/Loading";

const SingleMobilePhoneDetails = ({ status, mobilePhone }) => {
  return (
    <>
      {status === "loading" && <Loading />}
      {mobilePhone && mobilePhone.gpu && (
        <>
          <MobilePhoneImage>
            <img
              src={mobilePhone.imgUrl}
              alt={`${mobilePhone.brand} - ${mobilePhone.model}`}
            />
          </MobilePhoneImage>
          <Divider></Divider>
          <MobilePhoneDetails>
            <MobilePhoneModel>{`${mobilePhone.brand} - ${mobilePhone.model}`}</MobilePhoneModel>
            <MobilePhoneImageResponsive>
              <img
                src={mobilePhone.imgUrl}
                alt={`${mobilePhone.brand} - ${mobilePhone.model}`}
              />
            </MobilePhoneImageResponsive>
            <MobilePhoneListsWrapper>
              <ul>
                <li>
                  <MobilePhoneDescriptionName>CPU:</MobilePhoneDescriptionName>{" "}
                  <MobilePhoneDescription>
                    {mobilePhone.cpu}
                  </MobilePhoneDescription>
                </li>
                <li>
                  <MobilePhoneDescriptionName>RAM:</MobilePhoneDescriptionName>{" "}
                  <MobilePhoneDescription>
                    {mobilePhone.ram}
                  </MobilePhoneDescription>
                </li>
                <li>
                  <MobilePhoneDescriptionName>
                    Sistema Operativo:
                  </MobilePhoneDescriptionName>{" "}
                  <MobilePhoneDescription>
                    {mobilePhone.os}
                  </MobilePhoneDescription>
                </li>
                <li>
                  <MobilePhoneDescriptionName>
                    Resolucion de pantalla:
                  </MobilePhoneDescriptionName>{" "}
                  <MobilePhoneDescription>
                    {mobilePhone.displayResolution}
                  </MobilePhoneDescription>
                </li>
                <li>
                  <MobilePhoneDescriptionName>
                    Precio:
                  </MobilePhoneDescriptionName>{" "}
                  <MobilePhoneDescription>
                    {mobilePhone.price}€
                  </MobilePhoneDescription>
                </li>
              </ul>
              <ul>
                <li>
                  <MobilePhoneDescriptionName>
                    Bateria:
                  </MobilePhoneDescriptionName>{" "}
                  <MobilePhoneDescription>
                    {mobilePhone.battery}
                  </MobilePhoneDescription>
                </li>
                <li>
                  <MobilePhoneDescriptionName>
                    Camaras:{" "}
                  </MobilePhoneDescriptionName>
                  <MobilePhoneDescription>
                    - {mobilePhone.primaryCamera[0]}{" "}
                    {mobilePhone.primaryCamera[1]}
                  </MobilePhoneDescription>
                </li>
                <li>
                  <MobilePhoneDescriptionName>
                    Dimensiones:
                  </MobilePhoneDescriptionName>{" "}
                  <MobilePhoneDescription>
                    {mobilePhone.dimentions}
                  </MobilePhoneDescription>
                </li>
                <li>
                  <MobilePhoneDescriptionName>
                    Peso:{" "}
                  </MobilePhoneDescriptionName>
                  <MobilePhoneDescription>
                    {mobilePhone.weight}
                  </MobilePhoneDescription>
                </li>
              </ul>
            </MobilePhoneListsWrapper>
            <MobilePhoneDetailsActions
              id={mobilePhone.id}
              storages={mobilePhone.internalMemory}
              colors={mobilePhone.colors}
            />
          </MobilePhoneDetails>
        </>
      )}
    </>
  );
};

const MobilePhoneImage = styled.div`
  padding: 20px 50px;
  width: 200px;
  @media (max-width: 736px) {
    display: none;
  }
`;
const MobilePhoneImageResponsive = styled.div`
  display: none;
  padding: 20px 50px;
  width: 200px;
  @media (max-width: 736px) {
    display: block;
  }
`;
const Divider = styled.div`
  border: 1px solid #d1d5db;
  height: 350px;
  @media (max-width: 736px) {
    display: none;
  }
`;
const MobilePhoneDetails = styled.div`
  padding: 20px 50px;
  text-align: left;
`;
const MobilePhoneModel = styled.div`
  font-size: 30px;
  font-weight: 700;
  color: #111827;
`;
const MobilePhoneListsWrapper = styled.div`
  display: flex;
  > ul {
    list-style-type: none;
  }
  > ul:first-of-type {
    padding: 0;
  }
  @media (max-width: 736px) {
    flex-direction: column;
    > ul {
      padding: 0;
    }
  }
`;
const MobilePhoneDescriptionName = styled.div`
  font-size: 12px;
  font-weight: 700;
  color: #111827;
  padding-top: 8px;
`;
const MobilePhoneDescription = styled.div`
  font-size: 12px;
  color: #111827;
`;

export default SingleMobilePhoneDetails;

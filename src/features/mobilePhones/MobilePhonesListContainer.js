import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchMobilePhones,
  getMobilePhonesFilteredBySearchTerm,
} from "./mobilePhonesSlice";
import MobilePhonesList from "./MobilePhonesList";

const MobilePhonesListContainer = () => {
  const dispatch = useDispatch();
  const status = useSelector((state) => state.mobilePhones.status);
  const mobilePhones = useSelector(getMobilePhonesFilteredBySearchTerm);

  useEffect(() => {
    if (status === "idle") dispatch(fetchMobilePhones());
  }, [status, dispatch]);

  return (
    <>
      <MobilePhonesList status={status} mobilePhones={mobilePhones} />
    </>
  );
};

export default MobilePhonesListContainer;

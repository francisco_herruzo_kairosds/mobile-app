import styled from "styled-components";
import { useHistory } from "react-router-dom";

const MobilePhoneCard = ({ id, img, brand, model, price }) => {
  const history = useHistory();
  const handleClick = () => history.push(`/details/${id}`);

  return (
    <MobilePhoneCardWrapper data-testid="MobilePhoneCard" onClick={handleClick}>
      <MobilePhoneImage>
        <img src={img} alt={`${brand} - ${model}`} />
      </MobilePhoneImage>
      <MobilePhoneDetails>
        <MobilePhoneName>
          <MobilePhoneBrand>{brand}</MobilePhoneBrand>
          <MobilePhoneModel>{model}</MobilePhoneModel>
        </MobilePhoneName>
        <MobilePhonePriceDetails>
          <MobilePhonePriceWord>por </MobilePhonePriceWord>
          <MobilePhonePriceNumber>{price}€</MobilePhonePriceNumber>
        </MobilePhonePriceDetails>
      </MobilePhoneDetails>
    </MobilePhoneCardWrapper>
  );
};

const MobilePhoneCardWrapper = styled.div`
  border: 1px solid #e2e8f0;
  width: 300px;
  display: flex;
  justify-content: space-between;
  margin: 20px 12px;
  padding: 10px 5px;
  border-radius: 8px;
  cursor: pointer;
  --tw-shadow: 0 10px 15px -3px rgba(0, 0, 0, 0.1),
    0 4px 6px -2px rgba(0, 0, 0, 0.05);
  box-shadow: var(--tw-ring-offset-shadow, 0 0 #0000),
    var(--tw-ring-shadow, 0 0 #0000), var(--tw-shadow);
`;

const MobilePhoneImage = styled.div`
  width: 120px;
  > img {
    height: 120px;
  }
`;

const MobilePhoneDetails = styled.div`
  text-align: left;
  width: 160px;
`;
const MobilePhoneName = styled.div`
  font-weight: bold;
  color: #1e293b;
  margin-bottom: 30px;
`;
const MobilePhoneBrand = styled.div`
  font-size: 20px;
  margin-bottom: 4px;
`;
const MobilePhoneModel = styled.div`
  font-size: 15px;
`;
const MobilePhonePriceDetails = styled.div`
  display: flex;
  align-items: baseline;
  font-size: 16px;
`;
const MobilePhonePriceWord = styled.div`
  color: #64748b;
`;
const MobilePhonePriceNumber = styled.div`
  color: #475569;
  font-weight: bold;
  margin-left: 5px;
  font-size: 22px;
`;

export default MobilePhoneCard;

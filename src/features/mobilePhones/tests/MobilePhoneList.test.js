import { render } from "@testing-library/react";
import MobilePhonesList from "../MobilePhonesList";
import mobilePhonesMock from "./mocks/mobilePhonesMock";

describe("Mobile Phones List", () => {
  it("should render the loading component if the status is loading", () => {
    const { getByTestId } = render(<MobilePhonesList status="loading" />);

    expect(getByTestId("Loader")).toBeInTheDocument();
  });
  it("should render the mobile phones list correctly", async () => {
    const { findAllByTestId } = render(
      <MobilePhonesList status="" mobilePhones={mobilePhonesMock} />
    );

    const MobilePhones = await findAllByTestId(/MobilePhoneCard/i);

    expect(MobilePhones).toHaveLength(5);
  });
});

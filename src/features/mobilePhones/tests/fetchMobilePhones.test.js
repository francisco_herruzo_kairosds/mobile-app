import { store } from "../../../app/store";

describe("Fetch Mobile Phones", () => {
  it("should return the initial state", () => {
    const state = store.getState().mobilePhones;

    expect(state.entities).toEqual({});
  });
});

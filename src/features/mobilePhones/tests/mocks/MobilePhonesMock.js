const mobilePhonesMock = [
  {
    id: "ZmGrkLRPXOTpxsU4jjAcv",
    brand: "Acer",
    model: "Iconia Talk S",
    price: "170",
    imgUrl:
      "https://front-test-api.herokuapp.com/images/ZmGrkLRPXOTpxsU4jjAcv.jpg",
  },
  {
    id: "cGjFJlmqNPIwU59AOcY8H",
    brand: "Acer",
    model: "Liquid Z6 Plus",
    price: "250",
    imgUrl:
      "https://front-test-api.herokuapp.com/images/cGjFJlmqNPIwU59AOcY8H.jpg",
  },
  {
    id: "8hKbH2UHPM_944nRHYN1n",
    brand: "Acer",
    model: "Liquid Z6",
    price: "120",
    imgUrl:
      "https://front-test-api.herokuapp.com/images/8hKbH2UHPM_944nRHYN1n.jpg",
  },
  {
    id: "xyPoqGJxYR4Nn3yVGQcfI",
    brand: "Acer",
    model: "Iconia Tab 10 A3-A40",
    price: "230",
    imgUrl:
      "https://front-test-api.herokuapp.com/images/xyPoqGJxYR4Nn3yVGQcfI.jpg",
  },
  {
    id: "ND1elEt4nqZrCeFflDUZ2",
    brand: "Acer",
    model: "Liquid X2",
    price: "230",
    imgUrl:
      "https://front-test-api.herokuapp.com/images/ND1elEt4nqZrCeFflDUZ2.jpg",
  },
];

export default mobilePhonesMock;

import { render } from "@testing-library/react";
import MobilePhoneCard from "../MobilePhoneCard";
import mobilePhonesMock from "./mocks/mobilePhonesMock";

describe("Mobile Phone Card", () => {
  it("should render correctly the card", () => {
    const { id, imgUrl, brand, model, price } = mobilePhonesMock[0];
    const { getByText } = render(
      <MobilePhoneCard
        key={id}
        id={id}
        img={imgUrl}
        brand={brand}
        model={model}
        price={price}
      />
    );

    expect(getByText(brand)).toBeInTheDocument();
    expect(getByText(model)).toBeInTheDocument();
    expect(getByText(`${price}€`)).toBeInTheDocument();
  });
});

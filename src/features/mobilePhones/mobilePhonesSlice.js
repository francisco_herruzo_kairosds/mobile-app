import {
  createSlice,
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
} from "@reduxjs/toolkit";
import { selectSearchTerm } from "../search/searchSlice";
import { MobilePhoneAPI } from "../../services/MobilePhoneAPI";

export const fetchMobilePhones = createAsyncThunk(
  "mobilesPhones/fetchMobilesPhones",
  async () => {
    const { data } = await MobilePhoneAPI.getAll();
    return data;
  }
);
export const fetchMobilePhoneById = createAsyncThunk(
  "mobilesPhones/fetchMobilePhoneById",
  async (mobilePhoneId) => {
    const { data } = await MobilePhoneAPI.getById(mobilePhoneId);
    return data;
  }
);

const mobilesPhonesAdapter = createEntityAdapter({
  selectId: (mobilePhone) => mobilePhone.id,
});

export const {
  selectAll: selectAllMobilePhones,
  selectById: selectMobilePhoneById,
} = mobilesPhonesAdapter.getSelectors((state) => state.mobilePhones);

export const getMobilePhonesFilteredBySearchTerm = createSelector(
  [selectAllMobilePhones, selectSearchTerm],
  (mobilePhones, searchTerm) =>
    mobilePhones.filter((mobilePhone) => {
      return (
        mobilePhone.brand.toLowerCase().includes(searchTerm) ||
        mobilePhone.model.toLowerCase().includes(searchTerm)
      );
    })
);

const mobilePhonesSlice = createSlice({
  name: "mobilePhones",
  initialState: mobilesPhonesAdapter.getInitialState({
    status: "idle",
    error: null,
  }),
  reducers: {},
  extraReducers: {
    [fetchMobilePhones.pending]: (state, action) => {
      state.status = "loading";
      state.error = null;
    },
    [fetchMobilePhones.fulfilled]: (state, action) => {
      if (state.status === "loading") {
        mobilesPhonesAdapter.upsertMany(state, action);
        state.status = "succeeded";
      }
    },
    [fetchMobilePhones.rejected]: (state, action) => {
      if (state.status === "loading") {
        state.status = "failed";
        state.error = action.payload;
      }
    },
    [fetchMobilePhoneById.pending]: (state, action) => {
      state.status = "loading";
      state.error = null;
    },
    [fetchMobilePhoneById.fulfilled]: (state, action) => {
      if (state.status === "loading") {
        mobilesPhonesAdapter.upsertOne(state, action);
        state.status = "succeeded";
      }
    },
    [fetchMobilePhoneById.rejected]: (state, action) => {
      if (state.status === "loading") {
        state.status = "failed";
        state.error = action.payload;
      }
    },
  },
});

export default mobilePhonesSlice.reducer;

export const { mobilesPhonesLoaded } = mobilePhonesSlice.actions;

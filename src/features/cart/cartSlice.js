import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { CartAPI } from "../../services/CartAPI";

export const addItemToCart = createAsyncThunk(
  "cart/addItemToCart",
  async ({ id, color, storage }) => {
    const { data } = await CartAPI.add({
      id,
      colorCode: color,
      storageCode: storage,
    });
    return data;
  }
);

const cartSlice = createSlice({
  name: "cart",
  initialState: { count: 0 },
  reducers: {},
  extraReducers: {
    [addItemToCart.fulfilled]: (state, action) => {
      state.count = state.count + action.payload.count;
    },
  },
});

export default cartSlice.reducer;

import { render, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import { store } from "../app/store";
import MobileCatalogue from "./MobileCatalogue";

describe("Mobile Phones Catalogue", () => {
  it("should render the title", () => {
    const { getByText } = render(
      <Provider store={store}>
        <MobileCatalogue />
      </Provider>
    );

    expect(getByText(/Mobile Catalogue/i)).toBeInTheDocument();
  });
  it("should update the search term when the user change the input search", () => {
    const searchTerm = "new search";
    const { getByPlaceholderText } = render(
      <Provider store={store}>
        <MobileCatalogue />
      </Provider>
    );
    const searchInput = getByPlaceholderText("Search");

    fireEvent.change(searchInput, { target: { value: searchTerm } });
    const state = store.getState().search;

    expect(state.value).toBe(searchTerm);
  });
});

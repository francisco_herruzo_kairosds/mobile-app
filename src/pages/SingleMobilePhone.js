import { useSelector } from "react-redux";
import { selectMobilePhoneById } from "../features/mobilePhones/mobilePhonesSlice";

import NavBar from "../app/components/NavBar/NavBar";
import Breadcrumb from "../app/components/Breadcrumb/Breadcrumb";
import logo from "../app/images/logo.svg";
import MobilePhoneDetailsContainer from "../features/mobilePhones/MobilePhoneDetailsContainer";

const SingleMobilePhone = ({ match }) => {
  const { mobilePhoneId } = match.params;
  const { brand, model } = useSelector((state) =>
    selectMobilePhoneById(state, mobilePhoneId)
  );

  return (
    <>
      <NavBar logo={logo}>
        <Breadcrumb>
          {brand} - {model}
        </Breadcrumb>
      </NavBar>
      <MobilePhoneDetailsContainer mobilePhoneId={mobilePhoneId} />
    </>
  );
};

export default SingleMobilePhone;

import styled from "styled-components";
import { useSelector, useDispatch } from "react-redux";
import NavBar from "../app/components/NavBar/NavBar";
import Breadcrumb from "../app/components/Breadcrumb/Breadcrumb";
import logo from "../app/images/logo.svg";
import MobilePhonesListContainer from "../features/mobilePhones/MobilePhonesListContainer";
import Heading from "../app/components/Heading/Heading";
import { modify, selectSearchTerm } from "../features/search/searchSlice";

const MobileCatalogue = () => {
  const dispatch = useDispatch();
  const searchTerm = useSelector(selectSearchTerm);

  return (
    <>
      <NavBar logo={logo}>
        <Breadcrumb>Mobile Catalogue</Breadcrumb>
      </NavBar>
      <div>
        <MobileCatalogueHeader>
          <Heading> Mobile Catalogue </Heading>
          <SearchInput
            placeholder="Search"
            value={searchTerm}
            onChange={(event) =>
              dispatch(modify(event.target.value.toLowerCase()))
            }
          />
        </MobileCatalogueHeader>
        <MobilePhonesListContainer />
      </div>
    </>
  );
};

const MobileCatalogueHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  margin: 0 2em;
  @media (max-width: 736px) {
    flex-direction: column;
    align-items: center;
  }
`;

const SearchInput = styled.input`
  color: #000;
  height: 36px;
  width: 300px;
  padding: 0 16px;
  border-radius: 3px;
  background-color: #fff;
  border: 1px solid #d1d5db;
  @media (max-width: 736px) {
    margin-bottom: 20px;
  }
`;

export default MobileCatalogue;

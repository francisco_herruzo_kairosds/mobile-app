import axios from "axios";
import { API_PATH } from "../app/constants";

const axiosInstance = axios.create({
  baseURL: API_PATH,
});

export const MobilePhoneAPI = {
  getAll: async function () {
    return await axiosInstance.request({
      method: "GET",
      url: `/api/product`,
    });
  },
  getById: function (mobilePhoneId) {
    return axiosInstance.request({
      method: "GET",
      url: `/api/product/${mobilePhoneId}`,
    });
  },
};

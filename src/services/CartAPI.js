import axios from "axios";
import { API_PATH } from "../app/constants";

const axiosInstance = axios.create({
  baseURL: API_PATH,
});

export const CartAPI = {
  add: async function (params) {
    return await axiosInstance.request({
      method: "POST",
      url: `/api/cart`,
      data: params,
    });
  },
};
